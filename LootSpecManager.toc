## Interface: 80300
## Author: Req
## Title: LootSpecManager
## Notes: Manages loot spec preferences for dungeon/raid bosses.
## SavedVariablesPerCharacter: LTSM

data.lua
gui.lua
core.lua
